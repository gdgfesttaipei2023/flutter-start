import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutterstart/utils/camera_screen.dart';
import 'package:flutterstart/utils/http.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Start',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Start'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String hint = "";
  getHint() async {
    var rtn = await get("https://www.google.com/");
    if (rtn.statusCode == 200) {
      hint = rtn.body;
    }
    setState(() {});
  }

  getCamera() async {
    var camera = await availableCameras();
    if (!mounted) return;
    var rtn = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => CameraScreen(camera.first)));
    print(rtn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Hint:',
            ),
            Text(
              hint,
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getCamera,
        tooltip: 'Hint',
        child: const Icon(Icons.info),
      ),
    );
  }
}
