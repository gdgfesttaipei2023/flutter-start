import 'dart:io';

import 'package:http/http.dart';
import 'package:http/io_client.dart';

IOClient baseClient() {
  HttpClient httpClient = HttpClient();
  return IOClient(httpClient);
}

Future<Response> get(String url, {Map<String, String>? headers}) async {
  return await baseClient().get(Uri.parse(url), headers: headers);
}

Future<Response> post(String url,
    {Map<String, String>? headers, Object? body}) async {
  return await baseClient().post(Uri.parse(url), headers: headers, body: body);
}

Future<Response> put(String url, {Map<String, String>? headers}) async {
  return await baseClient().put(Uri.parse(url), headers: headers);
}

Future<Response> delete(String url, {Map<String, String>? headers}) async {
  return await baseClient().delete(Uri.parse(url), headers: headers);
}
