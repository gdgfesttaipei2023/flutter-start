import 'package:encrypt/encrypt.dart';

String aesEncode(String content) {
  try {
    final key = Key.fromUtf8("1234567812345678");
    final encrypter = Encrypter(AES(key, mode: AESMode.cbc));
    final encrypted =
        encrypter.encrypt(content, iv: IV.fromUtf8("1234567812345678"));
    return encrypted.base64;
  } catch (err) {
    print("aes encode error:$err");
    return content;
  }
}

//aes解密
dynamic aesDecode(dynamic base64) {
  try {
    final key = Key.fromUtf8("1234567812345678");
    final encrypter = Encrypter(AES(key, mode: AESMode.cbc));
    return encrypter.decrypt64(base64, iv: IV.fromUtf8("1234567812345678"));
  } catch (err) {
    print("aes decode error:$err");
    return base64;
  }
}
