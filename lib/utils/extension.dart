import 'package:intl/intl.dart';

extension DateTimeExt on DateTime {
  String toFormatString({String pattern = "yyyy-MM-dd HH:mm:ss"}) {
    final dateFormat = DateFormat(pattern);
    try {
      return dateFormat.format(this);
    } catch (e) {
      return "";
    }
  }
}

extension IntExt on int {
  //TODO
}
