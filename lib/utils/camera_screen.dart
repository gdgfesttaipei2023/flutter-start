import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class CameraScreen extends StatefulWidget {
  final CameraDescription camera;

  const CameraScreen(this.camera, {super.key});

  @override
  State<CameraScreen> createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;
  String pngPath = "";
  @override
  void initState() {
    super.initState();
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.medium,
    );
    _initializeControllerFuture = _controller.initialize();
  }

  getPath() async {
    pngPath = (await getTemporaryDirectory()).path;
    pngPath += "/${DateTime.now().millisecondsSinceEpoch}.png";
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.center, children: [
      FutureBuilder<List<dynamic>>(
          future: Future.wait([_initializeControllerFuture, getPath()]),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return CameraPreview(_controller);
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          }),
      Positioned(
          bottom: 20,
          child: ElevatedButton(
              onPressed: () async {
                var f = await _controller.takePicture();
                await f.saveTo(pngPath);
                if (!mounted) return;
                Navigator.of(context).pop(f.path);
              },
              child: const Text("拍照")))
    ]);
  }
}
